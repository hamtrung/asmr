﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asmr.Gameplay
{
    using Utils;
    public class EffectManager : MonoBehaviour
    {
        public static EffectManager Instance;

        [Header("Particle Effect")]
        public ParticleEffect shapeMachineEffect;
        public ParticleEffect smokeEffect;
        public ParticleSystem winEffect;
        public ParticleEffect hitEffect;
        public ParticleEffect[] slashs;

        [Header("Sound Effect")]
        public SoundEffect soundEffectPrefab;
        public AudioClip cutLoopSound;
        public AudioClip cutEndSound;
        public AudioClip winSound;
        public AudioClip loseSound;
        public AudioClip clickSound;
        public AudioClip chainSawLoopSound;
        public AudioClip chainSawEndSound;

        public SoundEffect cutLoopSoundEffect { get; set; }
        public SoundEffect chainSawLoopEffect { get; set; }
        private void Awake()
        {
            Instance = this;
            Vibration.Init();
        }
        public void Vibrate()
        {
            Vibration.Vibrate(1000);
        }
        public void Vibration_Peek()
        {
            if (!DataManager.Instance.IsVibrationOn()) return;
            Vibration.VibratePeek();
        }
        public void Vibration_Pop()
        {
            if (!DataManager.Instance.IsVibrationOn()) return;
            Vibration.VibratePop();
        }
        public void SpawnShapeMachineEffect(Vector3 pos)
        {
            SpawnEffect(shapeMachineEffect, pos);
        }
        public void SpawnSmokeEffect(Vector3 pos)
        {
            SpawnEffect(smokeEffect, pos);
        }
        public void SpawnSlashEffect(int id, Vector3 pos)
        {
            if(id >=0 && id < slashs.Length)
            {
                SpawnEffect(slashs[id], pos);
            }
        }
        public void SpawnHitEffect(Vector3 pos)
        {
            SpawnEffect(hitEffect, pos);
        }
        public void SpawnLoopCutSound(Vector3 pos)
        {
            if (!DataManager.Instance.IsSoundOn()) return;
            cutLoopSoundEffect = SpawnSoundEffect(cutLoopSound, pos, loop: true);
        }
        public void SpawnChainSawEndSound(Vector3 pos) 
        {
            if (!DataManager.Instance.IsSoundOn()) return;
            SpawnSoundEffect(chainSawEndSound, pos);
        }
        public void SpawnChainSawLoopSound(Vector3 pos)
        {
            if (!DataManager.Instance.IsSoundOn()) return;
            chainSawLoopEffect = SpawnSoundEffect(chainSawLoopSound, pos, loop: true);
        }
        public void SpawnEndCutSound(Vector3 pos)
        {
            if (!DataManager.Instance.IsSoundOn()) return;
            SpawnSoundEffect(cutEndSound, pos);
        }
        public void SpawnWinSound(Vector3 pos)
        {
            if (!DataManager.Instance.IsSoundOn()) return;
            SpawnSoundEffect(winSound, pos);
        }
        public void SpawnLoseSound(Vector3 pos)
        {
            if (!DataManager.Instance.IsSoundOn()) return;
            SpawnSoundEffect(loseSound, pos);
        }
        public void SpawnClickSound(Vector3 pos)
        {
            if (!DataManager.Instance.IsSoundOn()) return;
            SpawnSoundEffect(clickSound, pos);
        }
        private void SpawnEffect(ParticleEffect effect, Vector3 pos)
        {
            string name = effect.gameObject.name;
            ParticleEffect fx = ObjectPool.Get<ParticleEffect>(name);
            if (!fx)
            {
                fx = Instantiate(effect, transform);
            }
            fx.transform.position = pos;
            fx.gameObject.SetActive(true);
            fx.Init(name);
        }
        private SoundEffect SpawnSoundEffect(AudioClip clip, Vector3 pos, float volume = 1f, float d = 0f, bool loop = false)
        {
            SoundEffect sound = ObjectPool.Get<SoundEffect>();
            if (!sound)
            {
                sound = Instantiate(soundEffectPrefab, transform);
            }
            sound.transform.position = pos;
            sound.gameObject.SetActive(true);
            sound.Init(clip, volume, d, loop);
            return sound;
        }
    }
}
