﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Asmr.Gameplay
{
    public class KnifeSlice : SliceBase
    {
        public Animator animator;
        public Transform slashPosition;
        public List<KnifeBlade> blades = new List<KnifeBlade>();
        private void Awake()
        {
            blades.ForEach(a =>
            {
                a.animator = animator;
            });
        }
        public override void Slice(int id, Action callback)
        {
            if (animator)
            {
                StartCoroutine(ie_slice(id, callback));
            }
            else
            {
                foreach (KnifeBlade blade in blades)
                {
                    blade.transform.DOLocalMoveY(-2f, 0.4f).SetEase(Ease.OutQuart).OnComplete(() =>
                    {
                        blade.transform.DOLocalMoveY(0f, 0.3f).SetEase(Ease.InQuart).OnComplete(() =>
                        {
                            callback?.Invoke();
                        });
                    });
                }
            }
        }

        private IEnumerator ie_slice(int id, Action callback)
        {
            animator.SetBool("Begin", true);
            yield return new WaitForSeconds(1.8f);
            //EffectManager.Instance.SpawnSlashEffect(id, slashPosition.position);
            //yield return new WaitForSeconds(0.3f);

            animator.SetInteger("Slice_id", id);
            animator.SetTrigger("Slice");
            yield return new WaitForSeconds(0.833f * 2f);
            callback?.Invoke();
            animator.SetBool("Begin", false);
        }
    }
}
