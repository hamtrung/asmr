﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asmr.Gameplay
{
    public class CylinderHit : MonoBehaviour
    {
        public SphereCollider sphereCollider;
        public bool hit = false;
        public Vector3 GetPosition()
        {
            return transform.position + new Vector3(Random.Range(-sphereCollider.radius, sphereCollider.radius), 0f, Random.Range(-sphereCollider.radius, sphereCollider.radius));
        }
        public void OnTriggerEnter(Collider other)
        {
            //Debug.Log(other.gameObject.name);
            hit = true;
        }
        public void OnTriggerExit(Collider other)
        {
            hit = false;
        }
    }
}
