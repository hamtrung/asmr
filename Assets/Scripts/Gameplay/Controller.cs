﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Asmr.Gameplay
{
    public class Controller : MonoBehaviour
    {
        public FixedTouchField fixedTouch;
        private bool canRotate = true;
        private void Update()
        {
            if (GameplayManager.Instance.CurrentShape)
            {
                if (fixedTouch.Pressed && GameplayManager.Instance.canPlay)
                {
                    Vector2 dir = fixedTouch.TouchDist;
                    if(dir.x != 0f && canRotate)
                        Rotate(dir.x < 0 ? 1f : -1f);
                }
            }
        }
        private void Rotate(float direction)
        {
            canRotate = false;
            Quaternion rot = Quaternion.LookRotation(direction * GameplayManager.Instance.CurrentShape.transform.right, Vector3.up);
            GameplayManager.Instance.CurrentShape.transform.DORotateQuaternion(rot, 0.4f).SetEase(Ease.InQuart).OnComplete(()=>
            {
                canRotate = true;
                GameplayManager.Instance.levelChecking.UpdateRotate(GameplayManager.Instance.CurrentShape.transform.rotation);
            });
        }
        public void RefreshRotate()
        {
            GameplayManager.Instance.CurrentShape.transform.rotation = Quaternion.identity;
            GameplayManager.Instance.levelChecking.UpdateRotate(Quaternion.identity);
        }
    }
}
