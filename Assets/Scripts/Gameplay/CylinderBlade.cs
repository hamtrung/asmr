﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BzKovSoft.ObjectSlicerSamples;

namespace Asmr.Gameplay
{
    public class CylinderBlade : MonoBehaviour
    {
        public void Action(Transform target, Transform current, BooleanRT intersection, BooleanRT subtract)
        {
            //GameplayManager.Instance.BooleanShape.obj1 = target;
            //GameplayManager.Instance.BooleanShape.obj2 = current;
            //GameplayManager.Instance.BooleanShape.ExecuteOnClick();

            //GameplayManager.Instance.CurrentShape = objectSlicer;

            //BooleanRT intersection = GameplayManager.Instance.GetBooleanObj(true);
            //BooleanRT subtract = GameplayManager.Instance.GetBooleanObj(false);

            intersection.obj1 = target;
            intersection.obj2 = current;
            intersection.ExecuteOnClick();

            if (subtract)
            {
                subtract.obj1 = target;
                subtract.obj2 = current;
                subtract.ExecuteOnClick();
                subtract.transform.position = GameplayManager.Instance.CurrentShape.transform.position;
                subtract.transform.rotation = GameplayManager.Instance.CurrentShape.transform.rotation;
                subtract.transform.localScale = Vector3.one;
                GameplayManager.Instance.gameplayCleaner.AddPoolingClean(new GameplayCleaner.CleanObj<BooleanRT>(1.5f, subtract));
            }

            intersection.transform.position = GameplayManager.Instance.CurrentShape.transform.position;
            intersection.transform.rotation = GameplayManager.Instance.CurrentShape.transform.rotation;
            intersection.transform.localScale = Vector3.one;
            GameplayManager.Instance.CurrentShape.gameObject.SetActive(false);
            GameplayManager.Instance.CurrentShape = AddComponentToShape(intersection);

            MeshFilter meshFilter = intersection.GetComponent<MeshFilter>();
            MeshRenderer meshRenderer = intersection.GetComponent<MeshRenderer>();
            if(meshFilter.mesh.subMeshCount == 1)
            {
                meshRenderer.materials = new Material[1] { GameplayManager.Instance.CurrentShape.DefaultSliceMaterial };
            }
            //Debug.Log("Submesh Count: " + meshFilter.mesh.subMeshCount);
            //meshFilter.mesh.RecalculateBounds();
            //meshFilter.mesh.RecalculateNormals();
            //meshFilter.mesh.RecalculateTangents();
            //meshFilter.mesh.Optimize();

        }
        private ObjectSlicerSample AddComponentToShape(BooleanRT shape)
        {
            ObjectSlicerSample objectSlicer = shape.GetComponent<ObjectSlicerSample>();
            if (!objectSlicer)
            {
                objectSlicer = shape.gameObject.AddComponent<ObjectSlicerSample>();
                objectSlicer.DefaultSliceMaterial = GameplayManager.Instance.CurrentShape.DefaultSliceMaterial;
                objectSlicer.Asynchronously = GameplayManager.Instance.CurrentShape.Asynchronously;
                objectSlicer.DelayBetweenSlices = GameplayManager.Instance.CurrentShape.DelayBetweenSlices;
            }
            return objectSlicer;
        }
    }
}
