﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asmr.Gameplay
{
    public class EnvironmentController : MonoBehaviour
    {
        public static EnvironmentController Instance;
        public Environment day;
        public Environment night;
        private void Awake()
        {
            Instance = this;
            night.Active();
        }

        [System.Serializable]
        public class Environment
        {
            public List<GameObject> activeObjects = new List<GameObject>();
            public List<GameObject> inactiveObjects = new List<GameObject>();
            public Light directionLight;
            public float intensity;
            public float fogDensity;
            public Color backgroundColor;

            public void Active()
            {
                activeObjects.ForEach(a => a.SetActive(true));
                inactiveObjects.ForEach(a => a.SetActive(false));
                directionLight.intensity = intensity;
                Camera.main.backgroundColor = backgroundColor;
                RenderSettings.fogColor = backgroundColor;
                RenderSettings.fogDensity = fogDensity;
            }
        }
    }
}
