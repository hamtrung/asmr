﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Asmr.Gameplay
{
    public class ShapeSlice : SliceBase
    {
        public List<CylinderBlade> outputConcepts = new List<CylinderBlade>();
        public LayerMask layerChecking;
        public float distaneChecking = 2f;
        public List<Transform> raycastCheckings = new List<Transform>();
        public CylinderBlade cylinderBlade { get; set; }
        public Transform shape;
        public Transform leftModule;
        public Transform rightModule;
        public Animator leftAnimator;
        public Animator rightAnimator;
        public Transform smokeFx;
        public Transform completedFx;
       
        private RaycastHit hit;
        public override void Refresh()
        {
            leftModule.localPosition = new Vector3(-6f, 0f, 0f);
            rightModule.localPosition = new Vector3(6f, 0f, 0f);
        }
        public override void Init()
        {
            leftModule.DOLocalMoveX(-3f, 2f).SetEase(Ease.OutQuart);
            rightModule.DOLocalMoveX(3f, 2f).SetEase(Ease.OutQuart);
        }
        public override void Slice(int id, Action callback)
        {
            if(CanCut())
            {
                Cut(id, callback);
            }
            else
            {
                StartCoroutine(UnCut(callback));
            }
        }
        private IEnumerator UnCut(Action callback)
        {
            leftModule.DOLocalMoveX(-2.5f, 0.4f).SetEase(Ease.OutQuart);
            rightModule.DOLocalMoveX(2f, 0.4f).SetEase(Ease.OutQuart);
            yield return new WaitForSeconds(0.4f);
            EffectManager.Instance.Vibration_Pop();
            transform.DOShakeScale(0.25f, strength: 0.25f);
            transform.DOShakePosition(0.25f, strength: 0.1f);
            yield return new WaitForSeconds(0.25f);
            leftModule.DOLocalMoveX(-3f, 0.3f).SetEase(Ease.InQuart);
            rightModule.DOLocalMoveX(3f, 0.3f).SetEase(Ease.InQuart);
            yield return new WaitForSeconds(0.3f);
            callback?.Invoke();
        }
        private void Cut(int id, Action callback)
        {
            //shape.rotation = GameplayManager.Instance.CurrentShape.transform.rotation;
            LoadOutputConcept(id);
            BooleanRT intersection = GameplayManager.Instance.GetBooleanObj(true);
            leftModule.DOLocalMoveX(-0.6f, 0.4f).SetEase(Ease.OutQuart);
            rightModule.DOLocalMoveX(0.6f, 0.4f).SetEase(Ease.OutQuart).OnComplete(() =>
            {
                //cylinderBlade.Action(GameplayManager.Instance.CurrentShape.transform, mainShapeCollider.transform, intersection, null);
                //leftModule.DOLocalMoveX(-2f, 0.2f);
                //rightModule.DOLocalMoveX(2f, 0.2f);
                GameplayManager.Instance.CurrentShape.gameObject.GetComponent<MeshRenderer>().enabled = false;
                leftAnimator.SetTrigger("Run");
                rightAnimator.SetTrigger("Run");
                StartCoroutine(ie_slive(callback, intersection));
            });
        }
        private IEnumerator ie_slive(Action callback, BooleanRT intersection)
        {
            EffectManager.Instance.SpawnShapeMachineEffect(smokeFx.position);
            yield return new WaitForSeconds(4.5f);
            EffectManager.Instance.SpawnSmokeEffect(completedFx.position);
            cylinderBlade.Action(GameplayManager.Instance.CurrentShape.transform, cylinderBlade.transform, intersection, null);
            leftModule.DOLocalMoveX(-3f, 0.3f).SetEase(Ease.InQuart);
            rightModule.DOLocalMoveX(3f, 0.3f).SetEase(Ease.InQuart).OnComplete(()=>
            {
                callback?.Invoke();
            });
        }
        private void LoadOutputConcept(int id)
        {
            id = id - 40;
            cylinderBlade = outputConcepts[id];
        }

        private bool CanCut()
        {
            for(int i=0; i<raycastCheckings.Count; i++)
            {
                if(Physics.Raycast(raycastCheckings[i].position, raycastCheckings[i].forward.normalized, out hit, distaneChecking, layerChecking))
                {
                    return false;
                }
            }
            return true;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            for (int i=0; i<raycastCheckings.Count; i++)
            {
                Gizmos.DrawSphere(raycastCheckings[i].position, 0.05f);
                Gizmos.DrawRay(raycastCheckings[i].position, raycastCheckings[i].forward.normalized * distaneChecking);
            }
        }
    }
}