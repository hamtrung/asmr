﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asmr.Gameplay
{
    public class RaycastChecking : MonoBehaviour
    {
        public BoxCollider meshCollider;
        //public bool debug = false;
        public LayerMask layerChecking;
        public float distance = 10f;

        //private Vector3 pos = Vector3.zero;
        private bool check = false;
        private bool isSample = false;
        public bool SetCheck { set { check = value; } }
        public bool IsSample
        {
            get { return isSample; }
            set { isSample = value; }
        }
        private void OnDrawGizmos()
        {
            if (meshCollider && IsSample)
            {
                Gizmos.color = check ? Color.green : Color.red;
                //Gizmos.DrawSphere(transform.position, 0.05f);
                //Gizmos.DrawRay(transform.position, transform.forward.normalized * distance);
                //if(meshCollider) Gizmos.DrawCube(meshCollider.bounds.center, meshCollider.bounds.size);

                //Draw a Ray forward from GameObject toward the hit
                Gizmos.DrawRay(transform.position, transform.forward * distance);
                //Draw a cube that extends to where the hit exists
                Gizmos.DrawWireCube(transform.position + transform.forward * distance, transform.localScale);
            }
        }
        public bool Check()
        {
            RaycastHit hit;
            if(Physics.Raycast(transform.position, transform.forward.normalized, out hit, distance, layerChecking))
            {
                return true;
            }
            return false;
        }
        public bool BoxCheck()
        {
            //RaycastHit hit;
            //Debug.Log(transform.gameObject.name + " " + transform.localScale + " " + meshCollider.bounds.size);
            Collider[] res = Physics.OverlapBox(meshCollider.bounds.center, transform.localScale / 2f, transform.rotation, layerChecking);
            //check = Physics.OverlapBox(meshCollider.bounds.center, transform.localScale, transform.rotation, layerChecking).Length > 0;
            check = res.Length > 0;
            //for(int i=0; i< res.Length; i++)
            //{
            //    Debug.Log(res[i].gameObject.name);
            //}
            return check;
        }
    }
}
