﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asmr.Gameplay
{
    public class GameplayCleaner : MonoBehaviour
    {

        private List<CleanObj<GameObject>> cleanObjs = new List<CleanObj<GameObject>>();
        private List<CleanObj<BooleanRT>> poolingObjs = new List<CleanObj<BooleanRT>>();
        private void Update()
        {
            for(int i=0; i<cleanObjs.Count; i++)
            {
                if (cleanObjs[i].UpdateTime())
                    cleanObjs.Remove(cleanObjs[i]);
            }
            for( int i=0; i<poolingObjs.Count; i++)
            {
                if (poolingObjs[i].UpdateTime())
                    poolingObjs.Remove(poolingObjs[i]);
            }
        }
        public void AddClean(CleanObj<GameObject> cleanObj)
        {
            cleanObjs.Add(cleanObj);
        }
        public void AddPoolingClean(CleanObj<BooleanRT> cleanObj)
        {
            poolingObjs.Add(cleanObj);
        }
        public class CleanObj<T>
        {
            private float time;
            private float maxTime;
            private T obj;
            public CleanObj() { }
            public CleanObj(float _maxTime, T _obj)
            {
                maxTime = _maxTime;
                obj = _obj;
            }
            public bool UpdateTime()
            {
                time += Time.deltaTime;
                if(time >= maxTime)
                {
                    if(obj is BooleanRT)
                    {
                        GameplayManager.Instance.TurnOffBooleanObj(obj as BooleanRT, false);
                    }
                    else
                    {
                        GameObject gameObj = (obj as GameObject);
                        gameObj.transform.DOScale(Vector3.zero, 0.7f).SetEase(Ease.InBounce).OnComplete(() =>
                        {
                            gameObj.gameObject.SetActive(false);
                        });
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
