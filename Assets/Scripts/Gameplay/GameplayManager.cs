﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BzKovSoft.ObjectSlicerSamples;
using System;
using DG.Tweening;

namespace Asmr.Gameplay
{
    using Utils;
    public class GameplayManager : MonoBehaviour
    {
        public static GameplayManager Instance;
        [SerializeField] private List<SliceBase> slices = new List<SliceBase>();
       
        private int currentStep = 0;
        public Action<bool> CanPlayCallback;
        public Controller controller { get; set; }
        public GameplayCleaner gameplayCleaner { get; set; }
        public ObjectSlicerSample currentShape { get; set; }
        public LevelChecking levelChecking { get; set; }
        public ObjectSlicerSample CurrentShape
        {
            get { return currentShape; }
            set { currentShape = value; }
        }
        public bool canPlay { get; set; }
        public void SetCanPlay(bool val)
        {
            canPlay = val;
            CanPlayCallback?.Invoke(val);
        }
        private void Awake()
        {
            Instance = this;
            controller = GetComponent<Controller>();
            gameplayCleaner = GetComponent<GameplayCleaner>();
            levelChecking = GetComponentInChildren<LevelChecking>();
        }
        private void Start()
        {
            LoadTargetScreen();
        }
        public void LoadTargetScreen()
        {
            UI.GuiManager.Instance.LoadTargetScreen();
            CameraController.Instance.RefeshCamera();
        }
        public void LoadCurrentLevel()
        {
            DataManager.Instance.GenerateCurrentLevelData();
            EffectManager.Instance.winEffect.gameObject.SetActive(false);
            CameraController.Instance.TurnCuttingCamera(false);
            SetCanPlay(true);
            currentStep = 0;
            if (currentShape)
            {
                currentShape.transform.DOKill();
                currentShape.gameObject.SetActive(false);
            }
            levelChecking.LoadLevel();
            currentShape = Instantiate(DataManager.Instance.assetManager.GetFirstConcept(), transform);
            currentShape.transform.position = Vector3.zero;
            currentShape.transform.rotation = Quaternion.identity;
            currentShape.transform.localScale = Vector3.zero;
            currentShape.transform.DOScale(Vector3.one, 0.7f).SetEase(Ease.OutBounce);

            //List<int> leveData = DataManager.Instance.GetCurrentLevelData();
            List<int> leveData = DataManager.Instance.levelGenerated[currentStep];
            slices.ForEach((a) => a.gameObject.SetActive(false));
            foreach (List<int> lvs in DataManager.Instance.levelGenerated)
            {
                foreach (int slice_id in lvs)
                {
                    if (slice_id >= 0 && slice_id < 20)
                    {
                        slices[0].gameObject.SetActive(true);
                        slices[0].Refresh();
                    }
                    else if (slice_id >= 20 && slice_id < 40)
                    {
                        slices[1].gameObject.SetActive(true);
                        slices[1].Refresh();
                    }
                    else
                    {
                        slices[2].gameObject.SetActive(true);
                        slices[2].Refresh();
                    }
                }
            }
            UI.GuiManager.Instance.TurnOffAll();
            CameraController.Instance.OnBeginCamera(() =>
            {
                UI.GuiManager.Instance.LoadCurrentLevel(leveData);
                for(int i=0; i<slices.Count; i++)
                {
                    if (slices[i].gameObject.activeSelf)
                        slices[i].Init();
                }
            });
        }
        public void LoadCurrentSelections()
        {
            List<int> leveData = DataManager.Instance.levelGenerated[currentStep];
            //slices.ForEach((a) => a.gameObject.SetActive(false));
            //foreach (int slice_id in leveData)
            //{
            //    if (slice_id >= 0 && slice_id < 20)
            //    {
            //        slices[0].gameObject.SetActive(true);
            //        slices[0].Refresh();
            //    }
            //    else if (slice_id >= 20 && slice_id < 40)
            //    {
            //        slices[1].gameObject.SetActive(true);
            //        slices[1].Refresh();
            //    }
            //    else
            //    {
            //        slices[2].gameObject.SetActive(true);
            //        slices[2].Refresh();
            //    }
            //}
            UI.GuiManager.Instance.LoadCurrentLevel(leveData, true);
        }
        public void NextLevel()
        {
            DataManager.Instance.NextLevel();
            //LoadCurrentLevel();
            UI.GuiManager.Instance.LoadTargetScreen();
        }
        public void ExecuteSlice(int sliceIndex)
        {
            CameraController.Instance.TurnCuttingCamera(true);
            SetCanPlay(false);
            int id = 0;
            if (sliceIndex >= 0 && sliceIndex < 20) id = 0;         // cat doi
            else if (sliceIndex >= 20 && sliceIndex < 40) id = 1;   // cat tru
            else id = 2;                                            // cat hinh
            slices[id].Slice(sliceIndex,
            () =>
            {
                CameraController.Instance.TurnCuttingCamera(false);
                EndgameChecking(sliceIndex, (isWin) =>
                {
                    EffectManager.Instance.Vibrate();
                    UI.GuiManager.Instance.TurnOffAll();
                    currentShape.transform.DORotate(new Vector3(0, -360f, 0), 5f, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
                    if (isWin)
                    {
                        EffectManager.Instance.winEffect.gameObject.SetActive(true);
                        EffectManager.Instance.SpawnWinSound(transform.position);
                    }
                    else
                    {
                        EffectManager.Instance.SpawnLoseSound(transform.position);
                    }
                    CameraController.Instance.OnEndCamera(() =>
                    {
                        UI.GuiManager.Instance.OnEndgame(isWin);
                    });
                });
            });
        }
        public BooleanRT GetBooleanObj(bool isIntersection)
        {
            BooleanRT result = ObjectPool.Get<BooleanRT>(isIntersection ? "Intersection" : "Subtract");
            if (!result)
            {
                result = Instantiate(isIntersection ? DataManager.Instance.assetManager.intersectionObj : DataManager.Instance.assetManager.subtractObj, transform);
            }
            result.gameObject.SetActive(true);
            return result;
        }
        public void TurnOffBooleanObj(BooleanRT obj, bool isIntersection)
        {
            obj.transform.DOScale(Vector3.zero, 0.7f).SetEase(Ease.InBounce).OnComplete(() =>
            {
                obj.gameObject.SetActive(false);
                ObjectPool.Set<BooleanRT>(isIntersection ? "Intersection" : "Subtract", obj);
            });
        }
        public void EndgameChecking(int sliceIndex, Action<bool> callback)
        {
            //if(DataManager.Instance.GetCurrentCheckingLevel()[currentStep] == sliceIndex)
            //{
            //    currentStep += 1;
            //    if(currentStep == DataManager.Instance.GetCurrentCheckingLevel().Count)
            //    {
            //        callback?.Invoke(true);
            //    }
            //}
            //else
            //{
            //    callback?.Invoke(false);
            //}
            //if(levelCheckings[DataManager.Instance.userData.currentLevel].CheckWin())
            //    callback?.Invoke(true);

            currentStep += 1;
            levelChecking.CheckLevel((iswin) => 
            {
                if (iswin)
                {
                    callback?.Invoke(true);
                }
                else
                {
                    if (currentStep == DataManager.Instance.GetCurrentLevelData().Count)
                    {
                        callback?.Invoke(false);
                    }
                    else
                    {
                        LoadCurrentSelections();
                    }
                }
                SetCanPlay(true);
            });
              
            
        }
    }
}