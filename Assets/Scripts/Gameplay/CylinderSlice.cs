﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Asmr.Gameplay
{
    public class CylinderSlice : SliceBase
    {
        public MeshCollider mainShapeCollider;
        public CylinderBlade cylinderBlade;
        public Animator cylinderAnimator;
        public Transform circleHit;
        public CylinderHit hitPrefab;
        private List<CylinderHit> hits = new List<CylinderHit>();
        private bool Slicing = false;
        private bool Hit = false;
        private void Awake()
        {
            Slicing = false;
            Hit = false;
            SpawnCircleHit();
        }
        public override void Refresh()
        {
            cylinderBlade.transform.localPosition = new Vector3(0f, 4f, 0f);
        }
        public override void Init()
        { 
            cylinderBlade.transform.DOLocalMoveY(0f, 2f).SetEase(Ease.OutQuart);
        }
        public override void Slice(int id, Action callback)
        {
            Slicing = true;
            BooleanRT intersection = GameplayManager.Instance.GetBooleanObj(true);
            BooleanRT subtract = GameplayManager.Instance.GetBooleanObj(false);
            subtract.transform.position = GameplayManager.Instance.CurrentShape.transform.position;
            subtract.transform.rotation = GameplayManager.Instance.CurrentShape.transform.rotation;
            subtract.transform.localScale = Vector3.one;
            MeshRenderer mesh = subtract.gameObject.GetComponent<MeshRenderer>();
            Rigidbody rigidbody = subtract.gameObject.GetComponent<Rigidbody>();
            mesh.enabled = false;
            rigidbody.isKinematic = true;
            cylinderAnimator.SetBool("Run", true);
            EffectManager.Instance.SpawnChainSawLoopSound(transform.position);
            cylinderBlade.transform.DOLocalMoveY(-2f, 4f).SetEase(Ease.OutQuart).OnComplete(() =>
            {
                Hit = false;
                Slicing = false;
                mesh.enabled = true;
                rigidbody.isKinematic = false;
                cylinderAnimator.SetBool("Run", false);
                EffectManager.Instance.cutLoopSoundEffect?.TurnOff();
                EffectManager.Instance.chainSawLoopEffect?.TurnOff();
                EffectManager.Instance.SpawnEndCutSound(transform.position);
                cylinderBlade.Action(GameplayManager.Instance.CurrentShape.transform, mainShapeCollider.transform, intersection, subtract);
                cylinderBlade.transform.DOLocalMoveY(0f, 1f).SetEase(Ease.InQuart).OnComplete(() =>
                {
                    callback?.Invoke();
                });
            });
        }
        private void Update()
        {
            if (Slicing)
            {
                foreach(CylinderHit hit in hits)
                {
                    if (hit.hit)
                    {
                        EffectManager.Instance.SpawnHitEffect(hit.GetPosition());
                        if (!Hit)
                        {
                            EffectManager.Instance.SpawnLoopCutSound(transform.position);
                            Hit = true;
                        }
                    }
                }
            }
        }
        private void SpawnCircleHit()
        {
            int count = 20;
            float degree = 360 / 20;
            for(int i =0; i<count; i++)
            {
                CylinderHit hit = Instantiate(hitPrefab, circleHit);
                hit.transform.localPosition = Vector3.zero;
                hit.transform.localPosition += GetPosition(i, degree).normalized * 0.367f;
                hits.Add(hit);
            }
        }
        private Vector3 GetPosition(int stt, float degree)
        {
            degree = degree * stt;
            float x = Mathf.Cos(degree * Mathf.Deg2Rad);
            float y = Mathf.Sin(degree * Mathf.Deg2Rad);
            return new Vector3(x, 0f, y);
        }
    }
}
