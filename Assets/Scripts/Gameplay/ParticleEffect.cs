﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asmr.Gameplay
{
    using Utils;
    public class ParticleEffect : MonoBehaviour
    {
        public float timeLife = 3f;
        private string _name;
        public void Init(string _n)
        {
            _name = _n;
            StartCoroutine(ie_init());
        }
        private IEnumerator ie_init()
        {
            yield return new WaitForSeconds(timeLife);
            ObjectPool.Set(_name, this);
            gameObject.SetActive(false);
        }
    }
}
