﻿using Asmr.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

namespace Asmr.Gameplay
{
    public class LevelChecking : MonoBehaviour
    {
        public float startpos = -0.5f;
        public int size = 7;
        public float space = 0.14f;
        public Transform currentShape;
        public RaycastChecking checkingPrefab;
        public Transform localSpawn;
        public List<RaycastChecking> boxCheckings = new List<RaycastChecking>();
        public Transform checkingParent;
        private void Awake()
        {
            //LoadLevelConcept();
            space = (-2 * startpos) / (size - 1);
            //startpos = -1f * (checkingPrefab.transform.localScale.z * size + (space - checkingPrefab.transform.localScale.z) * (size - 1)) / 2f;
            float x = startpos, y = startpos, z = startpos;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    for (int u = 0; u < size; u++)
                    {
                        Vector3 localPosition = new Vector3(x + space * i, y + space * j, z + space * u);
                        RaycastChecking obj = Instantiate(checkingPrefab, checkingParent);
                        obj.transform.localPosition = localPosition;
                        boxCheckings.Add(obj);
                    }
                }
            }
           
        }
        public void SetGoal(Transform goal)
        {
            Mesh mesh = goal.GetComponent<MeshFilter>().mesh;
            currentShape.transform.localScale = goal.localScale;
            currentShape.transform.localRotation = goal.localRotation;
            currentShape.transform.localPosition = goal.localPosition;
            currentShape.GetComponent<MeshFilter>().mesh = mesh;
            currentShape.GetComponent<MeshCollider>().sharedMesh = mesh;
        }
        //private void SetupConcept()
        //{
        //    //currentShape = UI.GuiManager.Instance.targetObjectScreen.Goal;
        //    currentShape.DOKill();
        //    currentShape.SetParent(localSpawn);
        //    currentShape.localRotation = Quaternion.identity;
        //    currentShape.localPosition = Vector3.zero;
        //    currentShape.localScale = Vector3.one;
        //    SetLayer(currentShape, LayerMask.NameToLayer("Default"));
        //}
        private void SetLayer(Transform parent, int layer)
        {
            parent.gameObject.layer = layer;
            foreach (Transform child in parent)
            {
                SetLayer(child, layer);
            }
        }
        public void LoadLevel()
        {
            //SetupConcept();
            transform.rotation = Quaternion.identity;
            currentShape.gameObject.SetActive(true);
            for (int i = 0; i < boxCheckings.Count; i++)
            {
                boxCheckings[i].IsSample = boxCheckings[i].BoxCheck();
                boxCheckings[i].SetCheck = boxCheckings[i].IsSample;
            }
            currentShape.gameObject.SetActive(false);

            //Transform lastParent = checkingParent;
            //for (int i = 0; i < 3; i++)
            //{
            //    Transform obj = Instantiate(checkingParent, transform);
            //    obj.rotation = Quaternion.LookRotation(lastParent.right.normalized, Vector3.up);
            //    lastParent = obj;
            //}
        }
        public void UpdateRotate(Quaternion rotation)
        {
            transform.rotation = rotation;
        }
        public void CheckLevel(Action<bool> callback)
        {
            StartCoroutine(ie_checking(callback));
            //return GetLevelCheckingNumber() == 0;
        }
        private IEnumerator ie_checking(Action<bool> callback)
        {
            int minNum = int.MaxValue;
            Quaternion rotation = Quaternion.identity;
            for (int i = 0; i < 4; i++)
            {
                int num = GetLevelCheckingNumber();
                //Debug.Log(num);
                if (num < minNum)
                {
                    rotation = transform.rotation;
                    minNum = num;
                }
                transform.rotation = Quaternion.LookRotation(transform.right, Vector3.up);
                yield return new WaitForSeconds(0.2f);
            }
            transform.rotation = rotation;
            callback?.Invoke(minNum == 0);
        }
        private int GetLevelCheckingNumber()
        {
            int num1 = 0;
            for (int i = 0; i < boxCheckings.Count; i++)
            {
                if (boxCheckings[i].BoxCheck() != boxCheckings[i].IsSample)
                {
                    num1 += 1;
                    boxCheckings[i].SetCheck = false;
                }
                else
                {
                    boxCheckings[i].SetCheck = true;
                }
            }
            return num1;
        }
    }
}
