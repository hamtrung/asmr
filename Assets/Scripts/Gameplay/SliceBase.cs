﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Asmr.Gameplay
{
    public class SliceBase : MonoBehaviour
    {
        public virtual void Refresh()
        {

        }
        public virtual void Init()
        {

        }
        public virtual void Slice(int id, Action callback)
        {

        }
    }
}
