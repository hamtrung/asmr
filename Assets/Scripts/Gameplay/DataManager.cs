﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Asmr.Utils;
using Newtonsoft.Json;
using System;

namespace Asmr.Gameplay
{
    public class DataManager : Singleton<DataManager>
    {
        public AssetManager assetManager;
        public TextAsset levelDataJson;
        public TextAsset levelCheckingJson;
        public Action<int> addCoinCallback;
        public UserData userData { get; set; }
        public List<List<int>> levelDatas { get; set; }
        public List<List<int>> levelCheckings { get; set; }
        public List<List<int>> levelGenerated { get; set; }
        public override void OnAwake()
        {
            base.OnAwake();
            LoadLevelData();
            LoadUserData();
            Application.targetFrameRate = 60;
        }
        public void AddCoinCallbackFunc(Action<int> func)
        {
            addCoinCallback += func;
            addCoinCallback?.Invoke(userData.coin);
        }
        public void RemoveCoinCallbackFunc(Action<int> func)
        {
            addCoinCallback -= func;
        }

        #region User data
        private void LoadUserData()
        {
            string json = PlayerPrefs.GetString("USER_DATA");
            if (!string.IsNullOrEmpty(json))
            {
                userData = JsonUtility.FromJson<UserData>(json);
            }
            else
            {
                userData = new UserData();
            }
        }
        public void SaveUserData()
        {
            string json = JsonUtility.ToJson(userData);
            PlayerPrefs.SetString("USER_DATA", json);
        }
        public void NextLevel()
        {
            //userData.currentLevel = Mathf.Min(userData.currentLevel + 1, levelDatas.Count - 1);
            userData.currentLevel = userData.currentLevel + 1;
            if (userData.currentLevel == levelDatas.Count)
                userData.currentLevel = 0;
                
            SaveUserData();
        }
        public bool IsSoundOn()
        {
            return PlayerPrefs.GetInt("SOUND", 1) == 1;
        }
        public bool IsVibrationOn()
        {
            return PlayerPrefs.GetInt("VIBRATION", 1) == 1;
        }
        public void TurnSound()
        {
            PlayerPrefs.SetInt("SOUND", IsSoundOn() ? 0 : 1);
        }
        public void TurnVibration()
        {
            PlayerPrefs.SetInt("VIBRATION", IsVibrationOn() ? 0 : 1);
        }
        public void AddCoin(int _count)
        {
            userData.coin += _count;
            addCoinCallback?.Invoke(userData.coin);
        }
        #endregion

        #region Level data
        private void LoadLevelData()
        {
            levelDatas = new List<List<int>>();
            levelCheckings = new List<List<int>>();

            JsonSerializerSettings setting = new JsonSerializerSettings();
            setting.NullValueHandling = NullValueHandling.Ignore;
            List<LevelData> rawDatas = new List<LevelData>();
            rawDatas = JsonConvert.DeserializeObject<List<LevelData>>(levelDataJson.ToString(), setting);

            foreach(LevelData levelData in rawDatas)
            {
                List<int> datas = new List<int>();
                if (levelData.slice_0 >= 0) datas.Add(levelData.slice_0);
                if (levelData.slice_1 >= 0) datas.Add(levelData.slice_1);
                if (levelData.slice_2 >= 0) datas.Add(levelData.slice_2);
                if (levelData.slice_3 >= 0) datas.Add(levelData.slice_3);
                levelDatas.Add(datas);
            }

            List<LevelData> checkingDatas = new List<LevelData>();
            checkingDatas = JsonConvert.DeserializeObject<List<LevelData>>(levelCheckingJson.ToString(), setting);
            foreach (LevelData levelData in checkingDatas)
            {
                List<int> datas = new List<int>();
                if (levelData.slice_0 >= 0) datas.Add(levelData.slice_0);
                if (levelData.slice_1 >= 0) datas.Add(levelData.slice_1);
                if (levelData.slice_2 >= 0) datas.Add(levelData.slice_2);
                if (levelData.slice_3 >= 0) datas.Add(levelData.slice_3);
                levelCheckings.Add(datas);
            }
        }
        public List<int> GetCurrentLevelData()
        {
            int id = Mathf.Min(levelDatas.Count - 1, userData.currentLevel);
            return levelDatas[id];
        }
        public List<int> GetCurrentCheckingLevel()
        {
            int id = Mathf.Min(levelDatas.Count - 1, userData.currentLevel);
            return levelCheckings[id];
        }
        public void GenerateCurrentLevelData()
        {
            levelGenerated = new List<List<int>>();
            List<int> currentLv = GetCurrentLevelData();
            for(int i=0; i<currentLv.Count; i++)
            {
                List<int> selections = assetManager.GetAllSelectionIds();
                List<int> step = new List<int>();
                step.Add(currentLv[i]);
                selections.Remove(currentLv[i]);
                for(int j=0; j< 2; j++)
                {
                    int id = selections[UnityEngine.Random.Range(0, selections.Count)];
                    step.Add(id);
                    selections.Remove(id);
                }
                
                levelGenerated.Add(step);
            }
        }
        #endregion

        [System.Serializable]
        public class UserData
        {
            public int currentLevel = 0;
            public int coin = 0;
        }

        public class LevelData
        {
            public LevelData()
            {
                Id = -1;
                slice_0 = -1;
                slice_1 = -1;
                slice_2 = -1;
                slice_3 = -1;
            }
            public int Id { get; set; }
            public int slice_0 { get; set; }
            public int slice_1 { get; set; }
            public int slice_2 { get; set; }
            public int slice_3 { get; set; }
        }
    }
}
