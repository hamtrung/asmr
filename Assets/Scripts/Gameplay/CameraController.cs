﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Cinemachine;
using System;

namespace Asmr.Gameplay
{
    public class CameraController : MonoBehaviour
    {
        public static CameraController Instance;
        public CinemachineVirtualCamera beginCinemachine;
        public CinemachineVirtualCamera cuttingCinemachine;

        private CinemachineTrackedDolly beginTracked;
        //private CinemachineTrackedDolly endTracked;
        private void Awake()
        {
            Instance = this;
            beginTracked = beginCinemachine.GetCinemachineComponent<CinemachineTrackedDolly>();
            //endTracked = endCineMachine.GetCinemachineComponent<CinemachineTrackedDolly>();
        }
        public void RefeshCamera()
        {
            beginTracked.m_PathPosition = 2f;
        }
        public void OnBeginCamera(Action callback)
        {
            //beginCinemachine.gameObject.SetActive(true);
            //endCineMachine.gameObject.SetActive(false);
            beginTracked.m_PathPosition = 2f;
            DOTween.To(() => { return beginTracked.m_PathPosition; }, (a) => { beginTracked.m_PathPosition = a; }, 0f, 1.5f).OnComplete(() =>
            {
                callback?.Invoke();
            });
        }
        public void OnEndCamera(Action callback)
        {
            //beginCinemachine.gameObject.SetActive(false);
            //endCineMachine.gameObject.SetActive(true);
            beginTracked.m_PathPosition = 0f;
            DOTween.To(() => { return beginTracked.m_PathPosition; }, (a) => { beginTracked.m_PathPosition = a; }, 2f, 1.5f).OnComplete(() =>
            {
                //endTracked.m_PathPosition = 0f;
                callback?.Invoke();
            });
        }
        public void TurnCuttingCamera(bool turn)
        {
            cuttingCinemachine.gameObject.SetActive(turn);
        }
    }
}
