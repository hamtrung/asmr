﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Asmr.Utils;

namespace Asmr.Gameplay
{
    public class SoundEffect : MonoBehaviour
    {
        public AudioSource audioSource;
        public void Init(AudioClip clip, float volume, float d, bool loop)
        {
            audioSource.clip = clip;
            audioSource.volume = volume;
            audioSource.spatialBlend = d;
            audioSource.loop = loop;
            audioSource.Play();
            if (!loop)
            {
                StartCoroutine(ie_TurnOff());
            }
        }
        private IEnumerator ie_TurnOff()
        {
            yield return new WaitForSeconds(audioSource.clip.length + 0.5f);
            TurnOff();
        }
        public void TurnOff()
        {
            if (audioSource.isPlaying)
                audioSource.Stop();

            ObjectPool.Set<SoundEffect>(this);
            gameObject.SetActive(false);
        }
    }
}
