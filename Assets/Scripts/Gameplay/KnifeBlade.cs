﻿using BzKovSoft.ObjectSlicer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Asmr.Gameplay
{
    public class KnifeBlade : MonoBehaviour
    {
		public Animator animator { get; set; }
		public int SliceID { get; private set; }
		Vector3 _prevPos;
		Vector3 _pos;

		[SerializeField] private Vector3 _origin = Vector3.down;
		[SerializeField] private Vector3 _direction = Vector3.up;
		public Vector3 Origin
		{
			get
			{
				Vector3 localShifted = transform.InverseTransformPoint(transform.position) + _origin;
				return transform.TransformPoint(localShifted);
			}
		}
		public Vector3 BladeDirection { get { return transform.rotation * _direction.normalized; } }
		public Vector3 MoveDirection { get { return (_pos - _prevPos).normalized; } }
		public Vector3 collisionPosition { get; set; }
		public List<CylinderHit> hits = new List<CylinderHit>();
		private void Awake()
        {
			hits = GetComponentsInChildren<CylinderHit>().ToList();
        }
		private void Update()
		{
			_prevPos = _pos;
			_pos = transform.position;
			if(animator.speed == 0.1f)
            {
				for(int i=0; i < hits.Count; i++)
                {
                    if (hits[i].hit)
                    {
						EffectManager.Instance.SpawnHitEffect(hits[i].GetPosition());
					}
                }
            }
		}
		public void BeginNewSlice()
		{
			SliceID = UnityEngine.Random.Range(int.MinValue, int.MaxValue);
		}
		private void OnDrawGizmos()
		{
			if (MoveDirection.sqrMagnitude > 0.01f)
			{
				Gizmos.color = Color.red;
				Gizmos.DrawRay(transform.position, MoveDirection);
			}
			Gizmos.color = Color.green;
			Gizmos.DrawRay(transform.position, BladeDirection * 5f);

			Gizmos.color = Color.yellow;
			Gizmos.DrawRay(transform.position, transform.right.normalized * 2f);

			Gizmos.color = Color.blue;
			Gizmos.DrawSphere(Origin, 0.2f);

			Gizmos.color = Color.green;
			Gizmos.DrawSphere(collisionPosition, 0.2f);
		}
		public void OnTriggerEnter(Collider other)
		{
			//Debug.Log("Enter " + other.gameObject.name);
			collisionPosition = GetCollisionPoint(other.transform);
			Plane plane = new Plane(transform.right.normalized, collisionPosition);
			IBzSliceableAsync _sliceableAsync = other.gameObject.GetComponentInParent<IBzSliceableAsync>();

			if (_sliceableAsync != null)
			{
				_sliceableAsync.Slice(plane, 0, (r) =>
				{
					if (!r.sliced) return;

					if (animator.speed == 1f)
					{
						EffectManager.Instance.SpawnLoopCutSound(transform.position);
						animator.speed = 0.1f;
					}

					Rigidbody rigNeg = r.outObjectNeg.GetComponent<Rigidbody>();
					Rigidbody rigPos = r.outObjectPos.GetComponent<Rigidbody>();

					rigNeg.isKinematic = true;
					//rigPos.isKinematic = false;

					//rigPos.AddForce((transform.right - transform.up).normalized * 100f, ForceMode.Acceleration);
					rigPos.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
					//MeshRenderer meshRenderer = rigNeg.GetComponent<MeshRenderer>();
					//meshRenderer.materials[meshRenderer.materials.Length - 1].SetColor("_BaseColor", Random.ColorHSV(0.5f, 1f, 0.5f, 1f, 0.5f, 1f));
					//meshFilter.mesh.RecalculateBounds();
					//meshFilter.mesh.RecalculateNormals();
					//meshFilter.mesh.Optimize();

					//GameplayManager.Instance.gameplayCleaner.AddClean(new GameplayCleaner.CleanObj<GameObject>(1.5f, rigPos.gameObject));
					StartCoroutine(ie_addForce(rigPos));
				});
			}
		}
		private IEnumerator ie_addForce(Rigidbody rigPos)
        {
			//yield return new WaitForSeconds(0.1f);
			yield return new WaitUntil(() => { return animator.speed == 1f; });
			rigPos.isKinematic = false;
			rigPos.AddForce((transform.right - transform.up).normalized * 150f, ForceMode.Acceleration);
			GameplayManager.Instance.gameplayCleaner.AddClean(new GameplayCleaner.CleanObj<GameObject>(1.5f, rigPos.gameObject));
		}
        private void OnTriggerExit(Collider other)
        {
			//Debug.Log("Exit " + other.gameObject.name);
			if (animator.speed == 0.1f)
			{
				EffectManager.Instance.cutLoopSoundEffect?.TurnOff();
				EffectManager.Instance.SpawnEndCutSound(transform.position);
				animator.speed = 1f;
			}
		}
        private Vector3 GetCollisionPoint(Transform box)
		{
			Vector3 distToObject = box.position - Origin;
			Vector3 proj = Vector3.Project(distToObject, BladeDirection);

			Vector3 collisionPoint = Origin + proj;
			return collisionPoint;
		}
	}
}
