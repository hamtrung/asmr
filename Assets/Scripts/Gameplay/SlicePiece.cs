﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asmr.Gameplay
{
    public class SlicePiece : MonoBehaviour
    {
        public float mTime = 1f;
        private MeshCollider meshCollider;
        private void Awake()
        {
            meshCollider = GetComponent<MeshCollider>();
        }
        private void OnEnable()
        {
            StartCoroutine(ie_OnEnable());
        }
        private IEnumerator ie_OnEnable()
        {
            meshCollider.isTrigger = true;
            yield return new WaitForSeconds(mTime);
            meshCollider.isTrigger = false;
        }
    }
}
