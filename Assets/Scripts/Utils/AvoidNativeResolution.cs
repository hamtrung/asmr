﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvoidNativeResolution : MonoBehaviour
{
    public int totalChange = 1;
    [Range(0f, 1f)] public float overideScale = 1f;
    [Range(30, 60)] public int perfectFps = 60;
    public bool fullScreen = true;

    private int originWidth;
    private int originHeight;
    private int currentWidth;
    private int currentHeight;
    public static int count = 0;

    private void Awake()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }
    private void Start()
    {
        if (count < totalChange)
        {
            originWidth = Screen.width;
            originHeight = Screen.height;
            currentWidth = (int)(originWidth * overideScale);
            currentHeight = (int)(originHeight * overideScale);
            Screen.SetResolution(currentWidth, currentHeight, fullScreen, perfectFps);
            Application.targetFrameRate = perfectFps;
            count += 1;
        }
    }
}
