﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BzKovSoft.ObjectSlicerSamples;
using System.Linq;

namespace Asmr.Utils
{
    [CreateAssetMenu(fileName = "AssetHolder", menuName = "Asrm/AssetManager")]
    public class AssetManager : ScriptableObject
    {
        public List<ObjectSlicerSample> firstConcepts = new List<ObjectSlicerSample>();
        public BooleanRT intersectionObj;
        public BooleanRT subtractObj;
        public List<GameObject> conceptUIs = new List<GameObject>();
        public List<SelectionAsset> selections = new List<SelectionAsset>();

        public GameObject GetConceptUI()
        {
            int id = Mathf.Min(conceptUIs.Count - 1, Gameplay.DataManager.Instance.userData.currentLevel);
            return conceptUIs[id];
        }
        public ObjectSlicerSample GetFirstConcept()
        {
            int id = Mathf.Min(firstConcepts.Count - 1, Gameplay.DataManager.Instance.userData.currentLevel);
            return firstConcepts[id];
        }
        public SelectionAsset GetSelectionAsset(int id)
        {
            return selections.Select(a => a).Where(a => a.id == id).FirstOrDefault();
        }
        public List<int> GetAllSelectionIds()
        {
            List<int> ids = new List<int>();
            selections.ForEach(a => ids.Add(a.id));
            return ids;
        }
        [System.Serializable]
        public class SelectionAsset
        {
            public string name;
            public int id;
            public Sprite icon;
        }
    }
}
