﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Asmr.UI
{
    public class PopupSetting : PopupBase
    {
        public GameObject onSoundToggle;
        public GameObject offSoundToggle;
        public GameObject onVibrationToggle;
        public GameObject offVibrationToggle;

        public override void Show()
        {
            RefreshToggleState();
        }
        private void RefreshToggleState()
        {
            bool isSoundOn = Gameplay.DataManager.Instance.IsSoundOn();
            bool isVibrationOn = Gameplay.DataManager.Instance.IsVibrationOn();
            onSoundToggle.SetActive(isSoundOn);
            offSoundToggle.SetActive(!isSoundOn);
            onVibrationToggle.SetActive(isVibrationOn);
            offVibrationToggle.SetActive(!isVibrationOn);
        }
        public override void Close()
        {
        }
        public void OnClick_Sound()
        {
            Gameplay.DataManager.Instance.TurnSound();
            RefreshToggleState();
        }
        public void OnClick_Vibration()
        {
            Gameplay.DataManager.Instance.TurnVibration();
            RefreshToggleState();
        }
        public void OnClick_Shop()
        {
            GuiManager.Instance.popupShop.gameObject.SetActive(true);
        }
    }
}
