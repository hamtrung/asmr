﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Asmr.UI
{
    using Utils;
    public class TargetObjectScreen : MonoBehaviour
    {
        public Transform localSpawn;
        private Transform goalObj;
        private int currentLevel;
        public Transform Goal { get
            {
                return goalObj;
            }
            set { goalObj = value; }
        }
        private void OnEnable()
        {
            RenderSettings.fog = false;
            StartCoroutine(ie_onEnable());
        }
        private IEnumerator ie_onEnable()
        {
            yield return new WaitUntil(() => { return Gameplay.DataManager.Instance != null; });
            LoadConcept();
            goalObj.localScale = Vector3.zero;
            goalObj.localEulerAngles = new Vector3(0f, 60f, 0f);
            goalObj.DOScale(Vector3.one, 1f).SetEase(Ease.OutBounce).OnComplete(() =>
            {
                goalObj.DOLocalRotate(new Vector3(0, -360f, 0), 10f, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
            });
        }
        private void OnDisable()
        {
            RenderSettings.fog = true;
            //if (goalObj)
            //{
            //    goalObj.DOKill();
            //    DisConcept();
            //}
        }
        public void OnTap()
        {
            Gameplay.GameplayManager.Instance.LoadCurrentLevel();
        }
        private void LoadConcept()
        {
            if (goalObj)
            {
                goalObj.DOKill();
                DisConcept();
            }
            currentLevel = Gameplay.DataManager.Instance.userData.currentLevel;
            GameObject obj = Gameplay.DataManager.Instance.assetManager.GetConceptUI();
            goalObj = ObjectPool.Get<Transform>("Sample_" + Gameplay.DataManager.Instance.userData.currentLevel);
            if (goalObj == null)
            {
                goalObj = Instantiate(obj, localSpawn).transform;
            }
            else
            {
                goalObj.SetParent(localSpawn);
            }
            SetLayer(goalObj, LayerMask.NameToLayer("UI"));
            goalObj.gameObject.SetActive(true);
            Gameplay.GameplayManager.Instance.levelChecking.SetGoal(goalObj.GetChild(0));
        }
        private void SetLayer(Transform parent, int layer)
        {
            parent.gameObject.layer = layer;
            foreach(Transform child in parent)
            {
                SetLayer(child, layer);
            }
        }
        private void DisConcept()
        {
            ObjectPool.Set("Sample_" + currentLevel, goalObj);
            goalObj.gameObject.SetActive(false);
        }
    }
}
