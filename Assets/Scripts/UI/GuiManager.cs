﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asmr.UI
{
    public class GuiManager : MonoBehaviour
    {
        public static GuiManager Instance;

        public FixedTouchField fixedTouch;

        [Header("Screen")]
        public MainScreen mainScreen;
        public EndgameScreen endgameScreen;
        public TargetObjectScreen targetObjectScreen;

        [Header("Popup")]
        public PopupSetting popupSetting;
        public PopupShop popupShop;

        private void Awake()
        {
            Instance = this;
        }
        public void TurnOffAll()
        {
            fixedTouch.TouchDist = Vector2.zero;
            fixedTouch.gameObject.SetActive(false);
            endgameScreen.gameObject.SetActive(false);
            mainScreen.gameObject.SetActive(false);
            targetObjectScreen.gameObject.SetActive(false);
        }
        public void LoadCurrentLevel(List<int> levelData, bool onlySelection = false)
        {
            fixedTouch.gameObject.SetActive(true);
            endgameScreen.gameObject.SetActive(false);
            mainScreen.gameObject.SetActive(true);
            mainScreen.LoadCurrentLevel(levelData, onlySelection);
        }
        public void OnEndgame(bool iswin)
        {
            fixedTouch.gameObject.SetActive(false);
            mainScreen.gameObject.SetActive(false);
            endgameScreen.gameObject.SetActive(true);
            endgameScreen.Show(iswin);
        }
        public void LoadTargetScreen()
        {
            TurnOffAll();
            targetObjectScreen.gameObject.SetActive(true);
        }
    }
}
