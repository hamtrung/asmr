﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;

namespace Asmr.UI
{
    public class EndgameScreen : MonoBehaviour
    {
        public GameObject winObj;
        public GameObject loseObj;
        public TextMeshProUGUI reward;

        public void Show(bool iswin)
        {
            winObj.SetActive(iswin);
            loseObj.SetActive(!iswin);
            if (iswin)
            {
                reward.text = "500";
            }
        }
        public void OnClick_Next()
        {
            Gameplay.DataManager.Instance.AddCoin(500);
            Gameplay.GameplayManager.Instance.NextLevel();
        }
        public void OnClick_Retry()
        {
            Gameplay.GameplayManager.Instance.LoadCurrentLevel();
        }
        public void OnClick_x2()
        {

        }
        public void OnClick_Get500()
        {
            if (Gameplay.DataManager.Instance.userData.coin >= 500) 
            {
                Gameplay.DataManager.Instance.AddCoin(-500);
                Gameplay.GameplayManager.Instance.NextLevel();
            }
        }
    }
}