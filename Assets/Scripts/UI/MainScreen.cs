﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;

namespace Asmr.UI
{
    using Gameplay;
    using System;

    public class MainScreen : MonoBehaviour
    {
        public TextMeshProUGUI coinText;
        public Image background;
        public Text levelText;
        public List<Selection> sliceBtns = new List<Selection>();
        private void OnEnable()
        {
            background.gameObject.SetActive(false);
            Gameplay.DataManager.Instance.AddCoinCallbackFunc(addCoinFunc);

        }
        private void OnDisable()
        {
            Gameplay.DataManager.Instance.RemoveCoinCallbackFunc(addCoinFunc);
        }

        private void addCoinFunc(int obj)
        {
            coinText.text = obj.ToString();
        }

        public void LoadCurrentLevel(List<int> levelData, bool onlySelection = false)
        {
            sliceBtns.ForEach((a) => a.gameObject.SetActive(false));
            for(int i=0; i<levelData.Count; i++)
            {
                sliceBtns[i].gameObject.SetActive(true);
                sliceBtns[i].LoadSelection(levelData[i]);
            }

            if(!onlySelection) StartCoroutine(ie_enable());
        }
        //public void UpdateSelectionStates()
        //{
        //    sliceBtns.ForEach((a) => a.gameObject.SetActive(false));

        //}
        private IEnumerator ie_enable()
        {
            //background.gameObject.SetActive(true);
            //Color color = background.color;
            //background.color = new Color(color.r, color.g, color.b, 0f);
            //DOTween.To(() => { return background.color.a; }, (a) => { background.color = new Color(color.r, color.g, color.b, a); }, 255f, 0.5f).OnComplete(() =>
            //{
            //    DOTween.To(() => { return background.color.a; }, (a) => { background.color = new Color(color.r, color.g, color.b, a); }, 0f, 0.5f);
            //});
            //yield return new WaitForSeconds(1f);
            //background.gameObject.SetActive(false);


            levelText.gameObject.SetActive(false);
            yield return new WaitUntil(() => { return DataManager.Instance != null; });
            levelText.gameObject.SetActive(true);
            levelText.text = "LEVEL. " + (DataManager.Instance.userData.currentLevel + 1);
            levelText.transform.localScale = Vector3.one * 2f;
            levelText.transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.InQuad);
            yield return new WaitForSeconds(2f);
            levelText.transform.DOScale(Vector3.zero, 0.3f).SetEase(Ease.OutQuad);
        }
        public void OnClick_Step(int id)
        {
            GameplayManager.Instance.ExecuteSlice(id);
        }
        public void OnClick_NextLv_Test()
        {
            GameplayManager.Instance.NextLevel();
        }
        public void OnClick_Setting()
        {
            GuiManager.Instance.popupSetting.gameObject.SetActive(true);
        }
    }
}
