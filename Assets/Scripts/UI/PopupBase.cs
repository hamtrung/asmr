﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Asmr.UI
{
    public class PopupBase : MonoBehaviour
    {
        public Image background;
        public Transform popup;
        private void OnEnable()
        {
            Show();
            popup.transform.localScale = Vector3.one * 1.2f;
            if (background)
            {
                background.color = new Color(background.color.r, background.color.g, background.color.b, 0f);
                DOTween.To(() => { return background.color.a; }, (a) => background.color = new Color(background.color.r, background.color.g, background.color.b, a), 0.5f, 0.3f);
            }
            popup.transform.DOScale(Vector3.one, 0.3f).SetEase(Ease.InQuart);
        }
        private void OnDisable()
        {
        }
        public virtual void Show() { }
        public virtual void Close() { }
        public void OnClick_Close()
        {
            Close();
            if (background)
            {
                DOTween.To(() => { return background.color.a; }, (a) => background.color = new Color(background.color.r, background.color.g, background.color.b, a), 0f, 0.3f);
            }
            popup.transform.DOScale(Vector3.zero, 0.3f).SetEase(Ease.OutQuart).OnComplete(() =>
            {
                gameObject.SetActive(false);
            });
        }
    }
}
