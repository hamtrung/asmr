﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Asmr.UI
{
    using System;
    using Utils;
    public class Selection : MonoBehaviour
    {
        public Button btn;
        public Image Icon;
        public Text content;
        private int selection_id = 0;
        private bool selected = false;
        public void LoadSelection(int id)
        {
            selected = false;
            btn.interactable = true;
            selection_id = id;
            AssetManager.SelectionAsset asset = Gameplay.DataManager.Instance.assetManager.GetSelectionAsset(id);
            if (asset != null)
            {
                //content.text = asset.name;
                content.gameObject.SetActive(false);
                Icon.sprite = asset.icon;
            }
        }
        public void OnClick_Btn()
        {
            if (Gameplay.GameplayManager.Instance.canPlay && !selected)
            {
                Gameplay.GameplayManager.Instance.ExecuteSlice(selection_id);
                btn.interactable = false;
                selected = true;
            }
        }
        private void OnEnable()
        {
            Gameplay.GameplayManager.Instance.CanPlayCallback += CanplayFunc;
        }
        private void OnDisable()
        {
            Gameplay.GameplayManager.Instance.CanPlayCallback -= CanplayFunc;
        }

        private void CanplayFunc(bool obj)
        {
            if (!selected)
            {
                btn.interactable = obj;
            }
            else
            {
                btn.interactable = false;
            }
        }
    }
}
